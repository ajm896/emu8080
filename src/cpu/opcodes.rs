use super::Cpu;
pub fn parse_opcode(op: &u8, arg1: Option<u8>, arg2: Option<u8>, cpu: Option<&mut Cpu>) {
    let double_arg: u16 = combine_bytes(arg1.unwrap(), arg2.unwrap());
    match op {
        0x00 => nop(cpu.unwrap()),
        0x01 => lxib(double_arg, cpu.unwrap()),
        0x31 => lxi(double_arg, cpu.unwrap()),
        0x40 => movbtob(cpu.unwrap()),
        0x41 => movbtoc(cpu.unwrap()),
        0x42 => movbtod(cpu.unwrap()),
        0x43 => movbtoe(cpu.unwrap()),
        0x44 => movbtoh(cpu.unwrap()),
        0x45 => movbtol(cpu.unwrap()),
        0x8c => adch(cpu.unwrap()),
        0xc3 => jmp(double_arg, cpu.unwrap()),
        0xc5 => pushb(cpu.unwrap()),
        0xd5 => pushd(cpu.unwrap()),
        0xe5 => pushh(cpu.unwrap()),
        0xf5 => pushpsw(cpu.unwrap()),
        _ => println!("unDef"),
    };
}

fn nop(cpu: &mut Cpu) {
    println!("NOP");
    cpu.inc_pc();
}

fn movbtob(cpu: &mut Cpu) {
    println!("MOV\t\tB,B");
    cpu.inc_pc();
}

fn movbtoc(cpu: &mut Cpu) {
    println!("MOV\t\tB, C");
    cpu.inc_pc();
}

fn movbtod(cpu: &mut Cpu) {
    println!("MOV\t\tB, D");
    cpu.inc_pc();
}

fn movbtoe(cpu: &mut Cpu) {
    println!("MOV\t\tB, E");
    cpu.inc_pc();
}

fn movbtoh(cpu: &mut Cpu) {
    println!("MOV\t\tB, H");
    cpu.inc_pc();
}

fn movbtol(cpu: &mut Cpu) {
    println!("MOV\t\tB, L");
    cpu.inc_pc();
}

fn jmp(addr: u16, cpu: &mut Cpu) {
    println!("JMP {:04X?}", addr);
    cpu.set_pc(addr);
}

fn pushpsw(cpu: &mut Cpu) {
    println!("PUSH\tPSW");
    cpu.inc_pc();
}

fn adch(cpu: &mut Cpu) {
    println!("ADC \tH");
    cpu.inc_pc();
}

fn pushb(cpu: &mut Cpu) {
    println!("PUSH\t B");
    cpu.inc_pc();
}

fn pushd(cpu: &mut Cpu) {
    println!("PUSH\t D");
    cpu.inc_pc();
}

fn pushh(cpu: &mut Cpu) {
    println!("PUSH\t H");
    cpu.inc_pc();
}

fn lxi(arg: u16, cpu: &mut Cpu) {
    println!("LXI\t{:04X?}", arg);
    cpu.set_sp(arg);
    cpu.inc_pc();
}

// Immediate to Register BC
// 	B <- byte 3, C <- byte 2
fn lxib(double_arg: u16, cpu: &mut Cpu) {
    let (two, three) = split_bytes(double_arg);
    cpu.set_c(two);
    cpu.set_b(three);
    cpu.inc_pc();
    cpu.inc_pc();
}

// Combine two byte little endian
// 0xAA 0xBB -> 0xBBAA
fn combine_bytes(first: u8, second: u8) -> u16 {
    (second as u16) << 8 | first as u16
}

// Seperate Little Endian Double
// 0xBBAA -> (AA,BB)
fn split_bytes(double: u16) -> (u8, u8) {
    let first = (double & 0x00ff) as u8;
    let second = ((double & 0xff00) >> 8) as u8;

    (first, second)
}
