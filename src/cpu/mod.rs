mod memory;
mod opcodes;

use memory::Memory;
#[derive(Debug)]
pub struct Cpu {
    pc: u16,
    sp: u16,
    a: u8,
    b: u8,
    c: u8,
    d: u8,
    e: u8,
    h: u8,
    l: u8,
    mem: Memory,
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            pc: 0,
            sp: 0,
            a: 0,
            b: 0,
            c: 0,
            d: 0,
            e: 0,
            h: 0,
            l: 0,
            mem: Memory::new(),
        }
    }

    pub fn init(&mut self) {
        self.mem.read_rom();
    }

    pub fn next(&mut self) {
        let op = self.mem.get_byte(self.pc);
        opcodes::parse_opcode(
            &op,
            Some(self.mem.get_byte(self.pc + 1)),
            Some(self.mem.get_byte(self.pc + 2)),
            Some(self),
        );
    }

    #[allow(dead_code)]
    pub fn print_state(&self) {
        print!("{:?}", self)
    }

    pub fn inc_pc(&mut self) {
        self.pc = self.pc + 1;
    }

    pub fn set_pc(&mut self, addr: u16) {
        self.pc = addr;
    }

    pub fn set_sp(&mut self, arg: u16) {
        self.sp = arg;
    }

    pub fn set_b(&mut self, arg: u8) {
        self.b = arg;
    }
    pub fn set_c(&mut self, arg: u8) {
        self.c = arg;
    }
}
