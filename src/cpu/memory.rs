#![allow(dead_code)]

use std::fmt;
use std::fs;

//ROM
const INVADERSH_START: u16 = 0x000;
const INVADERSH_END: u16 = 0x07ff;

const INVADERSG_START: u16 = 0x0800;
const INVADERSG_END: u16 = 0x0fff;

const INVADERSF_START: u16 = 0x1000;
const INVADERSF_END: u16 = 0x17ff;

const INVADERSE_START: u16 = 0x1800;
const INVADERSE_END: u16 = 0x1fff;

//RAM
const WORKRAM_START: u16 = 0x2000;
const WORKRAM_END: u16 = 0x23ff;

const VIDEORAM_START: u16 = 0x2400;
const VIDEORAM_END: u16 = 0x3fff;

const MIRRORRAM_START: u16 = 0x4000;
const MIRRORRAM_END: u16 = 0xffff;

pub struct Memory {
    pub mem: [u8; 0xffff],
}

impl Memory {
    pub fn new() -> Memory {
        Memory { mem: [0x0; 0xffff] }
    }
    pub fn init_memory(&mut self) {
        for i in 0x0..0xffff {
            self.mem[i] = 0;
        }
    }

    pub fn load_h(&mut self, bytes: Vec<u8>) {
        let mut i = INVADERSH_START;
        for b in bytes {
            self.mem[i as usize] = b;
            i = i + 1;
        }
    }

    pub fn load_f(&mut self, bytes: Vec<u8>) {
        let mut i = INVADERSF_START;
        for b in bytes {
            self.mem[i as usize] = b;
            i = i + 1;
        }
    }

    pub fn load_g(&mut self, bytes: Vec<u8>) {
        let mut i = INVADERSG_START;
        for b in bytes {
            self.mem[i as usize] = b;
            i = i + 1;
        }
    }

    pub fn load_e(&mut self, bytes: Vec<u8>) {
        let mut i = INVADERSE_START;
        for b in bytes {
            self.mem[i as usize] = b;
            i = i + 1;
        }
    }

    pub fn dump_mem(&self) {
        for (i, b) in self.mem.iter().enumerate() {
            print!("{:02X?} ", b);
            if i > 1 && (i + 1) % 16 == 0 {
                print!("\n");
            }
        }
    }

    pub fn get_byte(&self, loc: u16) -> u8 {
        self.mem[loc as usize]
    }

    pub fn get_jmp_arg(&self, loc: u16) -> u16 {
        ((self.get_byte(loc + 2) as u16) << 8) | (self.get_byte(loc + 1) as u16)
    }

    pub fn read_rom(&mut self) {
        let h_bytes: Vec<u8> = fs::read("invaders.h").unwrap();
        let f_bytes: Vec<u8> = fs::read("invaders.f").unwrap();
        let g_bytes: Vec<u8> = fs::read("invaders.g").unwrap();
        let e_bytes: Vec<u8> = fs::read("invaders.e").unwrap();

        self.load_h(h_bytes);
        self.load_f(f_bytes);
        self.load_g(g_bytes);
        self.load_e(e_bytes);

        //mem.dump_mem();

        //println!("{:02X?}", mem.get_byte(0x0004));

        //mem.dump_disasm();
    }
}

impl fmt::Debug for Memory {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.mem[..].fmt(f)
    }
}
