mod cpu;
use cpu::Cpu;
use std::{thread, time};

fn main() {
    let mut cpu = Cpu::new();
    cpu.init();
    loop {
        cpu.next();
        thread::sleep(time::Duration::from_secs(1));
    }
}
